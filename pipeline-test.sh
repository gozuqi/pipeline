#!/bin/bash -xv

INSTANCE_ROLE=oshiro-ec2-gameserver
# roleの代替
#export AWS_ACCESS_KEY_ID=
#export AWS_SECRET_ACCESS_KEY=
export AWS_DEFAULT_REGION=ap-northeast-1

# 東京のAZを定義
AVAILABILITYZONE_A=ap-northeast-1a
AVAILABILITYZONE_C=ap-northeast-1c

# parameter set
USER_NAME=Go
INSTANCE_TYPE=c3.large
AZ=C
KEY_NAME=m-engine-keypair
TEST_COUNT=100

# Jenkins Param --start--

PARAM_INSTANCE_NAME="${USER_NAME}-vertx-spot-"
LOCATION_NO=3

echo "---- DEBUG PRINT  ----"

echo "LOCATION_NO: " $LOCATION_NO
echo "PARAM_INSTANCE_NAME: " $PARAM_INSTANCE_NAME
echo "INSTANCE_TYPE: " $INSTANCE_TYPE

echo "---- DEBUG PRINT  ----"

# Jenkins Param -- end --

EC2_USER_NAME=ubuntu
AIM_ID=ami-4c1b0422

RUNDATE=`date "+%Y%m%d-%H%M%S"`
INSTANCE_NAME="${PARAM_INSTANCE_NAME}${RUNDATE}_${BUILD_NUMBER}_pipeline"

pwd

# network
ZONEA_SUBNET_ID=$(aws ec2 describe-subnets --filters Name=tag-value,Values="*${LOCATION_NO}ClusterSubnetA*" | jq -r .Subnets[].SubnetId)
ZONEC_SUBNET_ID=$(aws ec2 describe-subnets --filters Name=tag-value,Values="*${LOCATION_NO}ClusterSubnetC*" | jq -r .Subnets[].SubnetId)
SECURITY_GROUPS=$(aws ec2 describe-security-groups --filters Name=tag-value,Values="*${LOCATION_NO}GameServer" | jq -r .SecurityGroups[].GroupId)

# keys
rm -rf keys
mkdir -v keys
aws s3 sync s3://kiq-user-contents/keys/ keys/
chmod 600 keys/*
ls -la keys
PEM_KEY_LOCATION=$(pwd)/keys/${KEY_NAME}.pem
SSH_OPT="-o StrictHostKeyChecking=no -i ${PEM_KEY_LOCATION}"

# step01
logger -p user.info "step01"

cat <<EOT > /tmp/user-data.sh
#!/bin/sh
/etc/init.d/td-agent stop

echo "I was born."
EOT

BASE64ENC=`base64 /tmp/user-data.sh |  tr -d '\n'`

cat <<EOT > /tmp/ec2_run-instance_zoneA.json
{
    "ImageId": "${AIM_ID}",
    "KeyName": "${KEY_NAME}",
    "InstanceType": "${INSTANCE_TYPE}",
    "BlockDeviceMappings": [
        {
            "DeviceName": "/dev/sda1",
            "Ebs": {
                "VolumeSize": 32,
                "VolumeType": "gp2"
            }
        }
    ],
    "Monitoring": {
        "Enabled": false
    },
    "Placement": {
        "AvailabilityZone": "${AVAILABILITYZONE_A}"
    },
    "NetworkInterfaces": [
        {
            "DeviceIndex": 0,
            "SubnetId": "${ZONEA_SUBNET_ID}",
            "Description": "",
            "Groups": [
                "${SECURITY_GROUPS}"
            ],
            "DeleteOnTermination": true,
            "AssociatePublicIpAddress": true
        }
    ],
    "IamInstanceProfile": {
        "Name": "${INSTANCE_ROLE}"
    },
     "UserData": "${BASE64ENC}"
}
EOT

cat <<EOT > /tmp/ec2_run-instance_zoneC.json
{
    "ImageId": "${AIM_ID}",
    "KeyName": "${KEY_NAME}",
    "InstanceType": "${INSTANCE_TYPE}",
    "BlockDeviceMappings": [
        {
            "DeviceName": "/dev/sda1",
            "Ebs": {
                "VolumeSize": 32,
                "VolumeType": "gp2"
            }
        }
    ],
    "Monitoring": {
        "Enabled": false
    },
    "Placement": {
        "AvailabilityZone": "${AVAILABILITYZONE_C}"
    },
    "NetworkInterfaces": [
        {
            "DeviceIndex": 0,
            "SubnetId": "${ZONEC_SUBNET_ID}",
            "Description": "",
            "Groups": [
                "${SECURITY_GROUPS}"
            ],
            "DeleteOnTermination": true,
            "AssociatePublicIpAddress": true
        }
    ],
    "IamInstanceProfile": {
        "Name": "${INSTANCE_ROLE}"
    },
     "UserData": "${BASE64ENC}"
}
EOT

create_instance() {

    SIR_IDS=$(aws ec2 request-spot-instances \
		--spot-price 0.5 \
		--region ${AWS_DEFAULT_REGION} \
		--launch-specification file:///tmp/ec2_run-instance_zone$1.json \
		--query  SpotInstanceRequests[*].SpotInstanceRequestId \
		--output text  )

	sleep 60s

	aws ec2 create-tags --resources ${SIR_IDS[@]} --tags "Key=Name,Value=${INSTANCE_NAME}$1"

	sleep 60s

	INSTANCE_IDS=$(aws ec2 describe-spot-instance-requests \
		--spot-instance-request-ids ${SIR_IDS[@]} \
		--region ${AWS_DEFAULT_REGION} \
		--query SpotInstanceRequests[*].InstanceId \
		--output text )

	aws ec2 create-tags --tags "Key=Name,Value=${INSTANCE_NAME}$1" \
    	--resource ${INSTANCE_IDS[@]}  

	time aws ec2 wait instance-status-ok --instance-ids ${INSTANCE_IDS[@]}
}

create_instance "${AZ}"

host=($(aws ec2 describe-instances --output text --query "Reservations[*].Instances[].PrivateIpAddress" \
	--filters "Name=instance-state-name,Values=running" "Name=tag:Name, Values=${INSTANCE_NAME}"))

check_instance() {
	local f=0
	while [ $f -eq 0 ]
	do
		sleep 30
		f=$(ssh ${SSH_OPT} ${EC2_USER_NAME}@$1 "tail -1 /var/log/cloud-init-output.log \
			| grep '^Cloud-init.*finished' | wc -l")
	done
    
}

check_instance "${host}"

aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name, Values=${INSTANCE_NAME}"

########################
cat <<EOF > /tmp/pipeline.sh

if [ -d vertxcppserver ]; then
  cd vertxcppserver
  sudo git fetch
  sudo git checkout $VERTXBRANCH
  sudo git pull origin $VERTXBRANCH
else
  sudo git clone -b $VERTXBRANCH --depth 1 https://game-enginedev:sGyiT2Yu@bitbucket.org/kiq/vertxcppserver.git
  cd vertxcppserver
#  sudo git checkout $VERTXBRANCH
#  cd vertxcppserver
fi

if [ -d dummyserver ]; then
  cd dummyserver
  sudo git fetch
  sudo git checkout $OSHIROBRANCH
  sudo git pull origin $OSHIROBRANCH
else
  sudo git clone -b $OSHIROBRANCH --depth 1 https://game-enginedev:sGyiT2Yu@bitbucket.org/kiq/dummyserver.git
  cd dummyserver
#  sudo git checkout $OSHIROBRANCH
  
fi

ls -ltr


./dummyserver_build_run.sh

EOF


export AWS_DEFAULT_REGION=ap-northeast-1
EC2_USER_NAME=ubuntu


aws ec2 wait instance-status-ok --instance-ids ${INSTANCE_ID}

# keys
rm -rf keys
mkdir -v keys
aws s3 sync s3://kiq-user-contents/keys/ keys/
chmod 600 keys/*
ls -la keys

PEM_KEY_LOCATION=$(pwd)/keys/${KEY_NAME}.pem
SSH_OPT="-o StrictHostKeyChecking=no -i ${PEM_KEY_LOCATION}"

EC2_IPADDR=$(aws ec2 describe-instances --instance-ids "${INSTANCE_IDS}" --output text --query "Reservations[*].Instances[*].PrivateIpAddress")

scp ${SSH_OPT} /tmp/job04.sh ${EC2_USER_NAME}@${EC2_IPADDR}:/tmp/job04.sh
ssh ${SSH_OPT} ${EC2_USER_NAME}@${EC2_IPADDR} "sudo /bin/bash /tmp/job04.sh"

LOOP_COUNT=15
HTTP_RESPONSE=""

while [[ "${HTTP_RESPONSE}" != "Status OK" && ${LOOP_COUNT} -ne 0 ]] ; do
  LOOP_COUNT=`expr ${LOOP_COUNT} - 1 `
  sleep 30
  echo "wait ..."
  HTTP_RESPONSE=`curl --connect-timeout 5 http://${EC2_IPADDR}:8080/status`
  echo "HTTP_RESPONSE: ${HTTP_RESPONSE}"
done

if [ ${LOOP_COUNT} -eq 0 ] ; then
  echo "too many loops"
  exit -1
fi

echo "done"

echo "start testing"

sleep 5
echo $EC2_IPADDR 
echo "${SIR_IDS}"
echo "${INSTANCE_IDS}"

export CUCUMBER_HOST=$EC2_IPADDR

cd ~/work/script/test


#### globalparams & dynamoDB test
bundle exec cucumber features/GetGameInfo_global_params.feature 
bundle exec cucumber features/GetGameInfo_createTable.feature
bundle exec cucumber features/GetGameInfo_simple_ksv.feature 
bundle exec cucumber features/GetGameInfo_putItem.feature 
bundle exec cucumber features/GetGameInfo_batch.feature
bundle exec cucumber features/GetGameInfo_getItem.feature
bundle exec cucumber features/GetGameInfo_deleteTable.feature

#### Cash対応
#bundle exec cucumber features/GetGameInfo_Cash.feature 
#sleep 5s
#bundle exec ruby script/post.rb
#rm cash.txt
#bundle exec ruby script/test.rb


#### sqs対応
bundle exec cucumber features/GetGameInfo_sqs_logger.feature
bundle exec ruby script/sqs.rb
bundle exec cucumber features/GetGameInfo_sqs_batch_logger.feature

aws ec2 cancel-spot-instance-requests --spot-instance-request-ids "${SIR_IDS}"

sleep 30

aws ec2 terminate-instances --instance-ids "${INSTANCE_IDS}"


